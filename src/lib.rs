fn e1_taylor_coefficients(n: isize) -> Vec<f64> {
    use natural_constants::math::euler_mascheroni;
    if n < 0 {
        panic!("n ≥ 0 is required");
    }
    if n == 0 {
        return vec![];
    }
    if n == 1 {
        return vec![-euler_mascheroni];
    }
    let mut term = 1.0;
    let mut terms = vec![-euler_mascheroni, term];
    for k in 2..=n {
        let k = k as f64;
        term = -term * (k - 1.0) / (k * k);
        terms.push(term);
    }
    terms
}

fn evalpoly64_impl(ident: proc_macro2::Ident, coefficients: &[f64]) -> proc_macro2::TokenStream {
    use quote::quote;
    let mut code = quote::quote! { 0.0 };
    let mut cs = coefficients.iter().copied().collect::<Vec<_>>();
    cs.reverse();
    for (i, c) in cs.into_iter().enumerate() {
        code = if 0 == i {
            quote! { #c as f64 }
        } else {
            quote! { #c as f64 + #ident * (#code) }
        }
    }
    quote::quote! {{ let #ident = #ident as f64; #code }}
}

#[proc_macro]
pub fn evalpoly64(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use proc_macro2::{
        TokenStream,
        TokenTree::{Group, Ident, Literal, Punct},
    };
    let message = "evalpoly64 requires (ident, [f64]). eg let z = 2f64; evalpoly64!(z, [1, 2, 3])";
    let input = TokenStream::from(input);
    let vec = input.into_iter().collect::<Vec<_>>();
    let (z, n) = match &vec[..] {
        [Ident(z), Punct(_), Group(a)] => (z, a),
        [Ident(z), Punct(_), Group(a), Punct(_)] => (z, a),
        _ => panic!(message),
    };
    let ident = z.to_owned();
    let mut list: Vec<f64> = vec![];
    let mut lit = true;
    for t in n.stream() {
        if lit {
            match t {
                Literal(l) => list.push(l.to_string().parse().unwrap()),
                _ => panic!(message),
            }
        } else {
            match t {
                Punct(_) => {}
                _ => panic!(message),
            }
        }
        lit = !lit;
    }
    let gen = evalpoly64_impl(ident, &list);
    gen.into()
}

#[proc_macro]
pub fn e1_taylor64(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use proc_macro2::{
        TokenStream,
        TokenTree::{Ident, Literal, Punct},
    };
    let input = TokenStream::from(input);
    let vec = input.into_iter().collect::<Vec<_>>();
    let (z, n) = match &vec[..] {
        [Ident(z), Punct(_), Literal(n)] => (z, n.to_string().parse::<isize>().unwrap()),
        _ => panic!("e1_taylor64 requires (ident, isize). eg let z = 2f64; e1_taylor64!(z, 12)"),
    };
    let c = e1_taylor_coefficients(n);
    let ident = z.to_owned();
    let pl = evalpoly64_impl(ident.clone(), &c);
    let gen = quote::quote! {{
        let t = #ident as f64;
        let pl = #pl;
        pl - t.log(std::f64::consts::E)
    }};
    gen.into()
}
