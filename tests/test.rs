#[test]
fn test_evalpoly64() {
    use rust_macro_20201012::evalpoly64;
    let z = 1.0;
    let a = evalpoly64!(z, [1, 2, 3, 4]);
    assert_eq!(a, 10_f64);
    let z = 2.0;
    let a = evalpoly64!(z, [1, 2, 3, 4, 5]);
    assert_eq!(a, 129_f64);
}

#[test]
fn test_e1_taylor64() {
    use rust_macro_20201012::e1_taylor64;
    let z = 11.0;
    let a = e1_taylor64!(z, 12);
    assert_eq!(a, -243.64861400820863);
    let z = 9.0;
    let a = e1_taylor64!(z, 7);
    assert_eq!(a, 69.09974343123166);

    // julia のコードをもとにしているので
    // 実行結果も julia で生成したものと比較している

    // function E₁_taylor_coefficients(::Type{T}, n::Integer) where T<:Number
    //     n < 0 && throw(ArgumentError("$n ≥ 0 is required"))
    //     n == 0 && return T[]
    //     n == 1 && return T[-MathConstants.eulergamma]
    //     # iteratively compute the terms in the series, starting with k=1
    //     term::T = 1
    //     terms = T[-MathConstants.eulergamma, term]
    //     for k=2:n
    //         term = -term * (k-1) / (k * k)
    //         push!(terms, term)
    //     end
    //     return terms
    // end
    // macro E₁_taylor64(z, n::Integer)
    //     c = E₁_taylor_coefficients(Float64, n)
    //     taylor = Expr(:macrocall, Symbol("@evalpoly"),
    //         Base.LineNumberNode(@__LINE__, Symbol(@__FILE__)),
    //         :t, c...)
    //     quote
    //         let t = $(esc(z))
    //             $taylor - log(t)
    //         end
    //     end
    // end
    // function main()
    //     y = 11
    //     println(@E₁_taylor64 y 12)
    //     z = 9
    //     println(@E₁_taylor64 z 7)
    // end
    // main()
}
